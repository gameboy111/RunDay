# RunDay

#### 介绍
java小游戏--天天酷跑

#### 软件架构
java,多线程,gui

#### 运行图片

![输入图片说明](https://images.gitee.com/uploads/images/2020/0418/143708_9be8c20d_7463338.png "image-20200418142759650.png")


![输入图片说明](https://images.gitee.com/uploads/images/2020/0418/143724_d6adcb7a_7463338.png "image-20200418142819960.png")


![输入图片说明](https://images.gitee.com/uploads/images/2020/0418/143735_07c63924_7463338.png "image-20200418142832637.png")





#### 使用说明



1. 打开LoginFrame.java类!

![输入图片说明](https://images.gitee.com/uploads/images/2020/0418/143758_44995c82_7463338.png "image-20200418142941960.png")

2. 运行程序

  ![输入图片说明](https://images.gitee.com/uploads/images/2020/0418/143808_83c9277d_7463338.png "image-20200418143045380.png")

3. 用户名: admin

   密码: 123456

  ![输入图片说明](https://images.gitee.com/uploads/images/2020/0418/143817_63ca8a2b_7463338.png "image-20200418143056129.png")

