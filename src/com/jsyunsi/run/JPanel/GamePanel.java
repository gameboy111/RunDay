/**
 * 
 */
package com.jsyunsi.run.JPanel;

import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.Arrays;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import com.jsyunsi.run.Entity.Barrs;
import com.jsyunsi.run.Entity.Coin;
import com.jsyunsi.run.Entity.Person;
import com.jsyunsi.run.JFrame.GameFrame;
import com.jsyunsi.run.JFrame.GameOver;

/**
 * @ 陈东
 * @ 2018年7月13日
 * @ 上午9:21:46
 * @ 日常搬砖
 * 核心主面板 核心代码区域
 * 主界面的背景图片加载，玩家、障碍物加载，及核心逻辑的实现
 */
public class GamePanel extends JPanel implements KeyListener,MouseListener{
	//1、绘制背景图片，并且让背景图片循环滚动
	//1.1声明背景图片变量
	Image background,showFiled,coinFiled;
	//添加结束开始按钮
	Image start,stop;
	boolean ismove;
	//2.创建玩家对象。并显示在面板上，添加键盘事件
	//2.1声明对象
	Person person;
	//3、创建螃蟹
	//3.1
	Barrs[] barrs = {};			//障碍物无限生成
	Coin[] coin = {};
	int distance;
	
	//加载音乐
	File file;
	URL url;
	URI uri;
	AudioClip  boom;
	
	public GamePanel() {
		
		try {
			file = new File("sound/boom1.wav");
			uri = file.toURI();
			url = uri.toURL();
			boom = Applet.newAudioClip(url);
			
		} catch (MalformedURLException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		
		//2给person赋值
		person = new Person();
		//1、给background赋值
		background = new ImageIcon("image/cc.png").getImage();
		showFiled = new ImageIcon("image/a12.png").getImage();
		coinFiled = new ImageIcon("image/112.png").getImage();
		start = new ImageIcon("image/b1.png").getImage();
		stop = new ImageIcon("image/b2.png").getImage();
	}

	int x;
	public void paint(Graphics g) {
		
		super.paint(g);
		if(!ismove) {
			x -= 2;
			g.drawImage(background,x,0,GameFrame.WIDTH,GameFrame.HEIGHT,null);
			g.drawImage(background,x+GameFrame.WIDTH,0,GameFrame.WIDTH,GameFrame.HEIGHT,null);
		}else {
			g.drawImage(background,x,0,GameFrame.WIDTH,GameFrame.HEIGHT,null);
			g.drawImage(background,x+GameFrame.WIDTH,0,GameFrame.WIDTH,GameFrame.HEIGHT,null);
		}
		g.drawImage(showFiled,0,0,190,55,null);
		g.drawImage(showFiled,200,0,190,55,null);
		g.drawImage(showFiled,400,0,190,55,null);
		g.drawImage(start,100,470,60,60,null);
		g.drawImage(stop,200,470,60,60,null);

		String life = "生命:" + (person.getLife());
		Font f2 = new Font("宋体",Font.BOLD,30);
		g.setFont(f2);
		g.setColor(Color.white);
		g.drawString(life,15,40);
		
		String coins = "金币:" + (person.getScore());
		Font f1 = new Font("宋体",Font.BOLD,30);
		g.setFont(f1);
		g.setColor(Color.white);
		g.drawString(coins,415,40);
		
		person.setDistanceIndex(person.getDistanceIndex()+1);;
		String Distance = "距离:" + person.getDistanceIndex()/50 + "米";
		Font f3 = new Font("宋体",Font.BOLD,30);
		g.setFont(f3);
		g.setColor(Color.white);
		g.drawString(Distance,215,40);
		
		if(x <= -GameFrame.WIDTH) {
			x = 0;
		}
		
		//
		
		//2、3绘制玩家图片
		person.paintPerson(g);
		
		
		for(int i= 0;i < barrs.length;i++) {
			barrs[i].paintBarrs(g);
			}
			for(int i= 0;i < coin.length;i++) {
				coin[i].paintCoin(g);
			}
			
			
		
	}
	
	//物体移动的方法
	public void stepAction() {

		//2.4玩家移动
		person.step();
		person.drop();
		//3.3障碍物移动
		for(int i= 0;i < barrs.length;i++) {
			barrs[i].step();
		}
		//金币移动
			for(int i= 0;i < coin.length;i++) {
				coin[i].step();
		}
	}
	
	//3.2障碍物入场方法
	int index = 0;
	public void EnteredAction() {
		index++;
		if(index%700 == 0) {
			Barrs bar1 = new Barrs();					//生成螃蟹
			barrs = Arrays.copyOf(barrs, barrs.length+1);
			barrs [barrs.length-1] = bar1;				//放在内存的最后一个
		}
		if(index%30 == 0) {
			Coin coin1 = new Coin();					//生成金币
			coin = Arrays.copyOf(coin, coin.length+1);
			coin [coin.length-1] = coin1;				//放在内存的最后一个
		}
	}

	
	//玩家和障碍物碰撞的方法
	public void wardAction() {
		//玩家和螃蟹碰撞
		for(int i = 0;i < barrs.length;i++) {
			if(person.getY() + person.HEIGHT > barrs[i].getY()+44 && 
				person.getY() < barrs[i].getY() + barrs[i].getHeight() && 
				person.getX() + person.WIDTH > barrs[i].getX() && 
				person.getX() < barrs[i].getX() + barrs[i].getWidth()) {
				person.setX(barrs[i].getX() - person.WIDTH);
			}
		}
		
		//玩家和悬浮物碰撞
		for(int i = 0;i < barrs.length;i++) {
			if(person.getY() < barrs[i].getB() + barrs[i].getHeight()-64 && 
				person.getX() + person.WIDTH > barrs[i].getA() && 
				person.getX() < barrs[i].getA() + barrs[i].getWidth()) {
				person.setX(barrs[i].getA() - person.WIDTH);
			}
		}
		
		//玩家和导弹碰撞
		for(int i = 0;i < barrs.length;i++) {
			if(person.getY() + person.HEIGHT > barrs[i].getDy() && 
				person.getY() < barrs[i].getDy() + barrs[i].getHeight()-38 && 
				person.getX() + person.WIDTH > barrs[i].getDx()+22 && 
				person.getX() < barrs[i].getDx() + barrs[i].getWidth()) {
				person.setX(450);
				person.setY(315);
				barrs[i].setDx(-1500);
				barrs[i].setDy(barrs[i].getDy());
				person.setLife(person.getLife()-1);
				boom.play();
			}
		}
				
		//玩家遇到台阶
		for(int i = 0;i < barrs.length;i++) {
			if(/*person.getY() + person.HEIGHT >= barrs[i].getSy() && */
				person.getX() + person.WIDTH > barrs[i].getSx()+15 && 
				person.getX() < barrs[i].getSx() + barrs[i].getWidth()+22) {
				person.setY(barrs[i].getSy() - person.HEIGHT);
			}
			/*else if(person.getY() >= barrs[i].getSy() + barrs[i].getHeight() && 
				person.getX() + person.WIDTH > barrs[i].getSx() && 
				person.getX() < barrs[i].getSx() + barrs[i].getWidth()+22) {
				person.setY(315);
			}*/
		}
				
		
		//吃金币
		for(int i = 0;i < coin.length;i++) {
			if(person.getY() + person.HEIGHT > coin[i].getCy() && 
				person.getY() < coin[i].getCy() + coin[i].HEIGHT && 
				person.getX() + person.WIDTH > coin[i].getCx() && 
				person.getX() < coin[i].getCx() + coin[i].WIDTH) {
				//玩家分数增加
				int s = person.getScore();
				person.setScore(s+2);
				//金币消失
				coin[i] = coin[coin.length-1];
				coin = Arrays.copyOf(coin, coin.length-1);
			}
		}
		for(int i = 0;i < coin.length;i++) {
			if(person.getY() + person.HEIGHT > coin[i].getCsy() && 
				person.getY() < coin[i].getCsy() + coin[i].HEIGHT && 
				person.getX() + person.WIDTH > coin[i].getCsx() && 
				person.getX() < coin[i].getCsx() + coin[i].WIDTH) {
				//玩家分数增加
				int s = person.getScore();
				person.setScore(s+1);
				//金币消失
				coin[i] = coin[coin.length-1];
				coin = Arrays.copyOf(coin, coin.length-1);
			}
		}
		
	}

	
	//障碍物越界处理
	/*public void Dispear() {
		for(int i = 0;i < barrs.length;i++) {
			if(barrs[i].getX() + barrs[i].getWidth() <= 0 &&
				barrs[i].getA() + barrs[i].getWidth() <= 0 &&
				barrs[i].getDx() + barrs[i].getWidth() <= 0 &&
				barrs[i].getSx() + barrs[i].getWidth() <= 0) {
				barrs[i] = null;
				//barrs = Arrays.copyOf(barrs, barrs.length-1);	
			}
		}
		for(int i = 0;i < coin.length;i++) {
			if(coin[i].getCx() + coin[i].WIDTH <= 0) {
				coin[i] = null;
			}
		}
	}*/
	
	//程序结束的方法
	boolean flag = true;
	public static boolean isover = false;
	public void gameOver() {
		if(person.getX() <= -person.WIDTH || person.getLife() <= 0) {
			JOptionPane.showMessageDialog(null, "gameover");
			isover = true;
			flag = false;
			//打开结束界面
			new GameOver(person.getDistanceIndex()/50,person.getScore() + person.getDistanceIndex()/50);
			//数据初始化
			person  = new Person();
			barrs = new Barrs[0] ;
			coin = new Coin[0];
		}
		else if(person.getX() <= -person.WIDTH || person.getLife() > 0) {
			isover = false;
			flag = true;
		}
	}
	
	public void action() {
		new Thread() {
			public void run() {
				
				while(true) {
					if(flag) {
						stepAction();
						EnteredAction();
						wardAction();
						//Dispear();
						gameOver();
					}
					repaint();
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

				}
			}
		}.start();						//直接Start
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int x = person.getX();
		int y = person.getY();
		if(e.getKeyCode()==KeyEvent.VK_UP) {
			person.setY(y-80);
		}
		if(e.getKeyCode()==KeyEvent.VK_SPACE) {
			person.setX(x+130);
		}
		if(person.getY()<=-120) {
			person.setY(y);
		}
		if(e.getKeyCode()==KeyEvent.VK_RIGHT) {
			person.setX(x+80);
		}
		if(e.getKeyCode()==KeyEvent.VK_LEFT) {
			person.setX(x-80);
		}
		if(person.getX()<=0 || person.getX()>=950) {
			person.setX(x);
		}
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO 自动生成的方法存根
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO 自动生成的方法存根
		
	}

	public void mouseClicked(MouseEvent e) {
		if(e.getX()>=100 && e.getX()<=160 && e.getY()>=470 && e.getY()<=530) {
			flag = true;
			ismove = false;
		}
		if(e.getX()>=200 && e.getX()<=260 && e.getY()>=470 && e.getY()<=530) {
			flag = false;
			ismove = true;
		}
	}

	/* （非 Javadoc）
	 * @see java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
	 */
	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO 自动生成的方法存根
		
	}

	/* （非 Javadoc）
	 * @see java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent)
	 */
	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO 自动生成的方法存根
		
	}

	/* （非 Javadoc）
	 * @see java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
	 */
	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO 自动生成的方法存根
		
	}

	/* （非 Javadoc）
	 * @see java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
	 */
	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO 自动生成的方法存根
		
	}
	
}
