/**
 * 
 */
package com.jsyunsi.run.Entity;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;

import com.jsyunsi.run.JFrame.GameFrame;

/**
 * @ 陈东
 * @ 2018年7月14日
 * @ 上午10:53:23
 * @ 日常搬砖
 */
public class Coin {
	
	private BufferedImage[] coins;
	private BufferedImage coin1,coin2;
	private BufferedImage coin;
	private BufferedImage[] slivers;
	private BufferedImage sliver1,sliver2;
	private BufferedImage sliver;
	int cx,cy;					//金币的坐标
	int csx,csy;				//银币的坐标
	private int index;
	private int speed = 2;
	public static final int WIDTH = 37;
	public static final int HEIGHT = 37;
	Random random = new Random();

	public Coin() {
		initCoin();
		coins = new BufferedImage[] {coin1,coin2};
		slivers = new BufferedImage[] {sliver1,sliver2};
		cx = GameFrame.WIDTH + random.nextInt(13);
		cy = random.nextInt(255) + 50;
		csx = GameFrame.WIDTH + random.nextInt(13)+5;
		csy = random.nextInt(255) + 55;
	}
	
	//移动方法
	public void step() {
		coin = coins[index++/33%2];
		cx -= speed;
		sliver = slivers[index++/33%2];
		csx -= speed;
	}
	
	public void initCoin() {
		try {
			coin1 = ImageIO.read(new File("image/21.png"));
			coin2 = ImageIO.read(new File("image/24.png"));
			sliver1 = ImageIO.read(new File("image/25.png"));
			sliver2 = ImageIO.read(new File("image/26.png"));
		} catch (IOException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
	}
	
	//绘制
		public void paintCoin(Graphics g) {
			g.drawImage(coin, cx, cy, null);
			g.drawImage(sliver, csx, csy, null);
		}

		
		/**
		 * @return slivers
		 */
		public BufferedImage[] getSlivers() {
			return slivers;
		}

		/**
		 * @param slivers 要设置的 slivers
		 */
		public void setSlivers(BufferedImage[] slivers) {
			this.slivers = slivers;
		}

		/**
		 * @return sliver1
		 */
		public BufferedImage getSliver1() {
			return sliver1;
		}

		/**
		 * @param sliver1 要设置的 sliver1
		 */
		public void setSliver1(BufferedImage sliver1) {
			this.sliver1 = sliver1;
		}

		/**
		 * @return sliver2
		 */
		public BufferedImage getSliver2() {
			return sliver2;
		}

		/**
		 * @param sliver2 要设置的 sliver2
		 */
		public void setSliver2(BufferedImage sliver2) {
			this.sliver2 = sliver2;
		}

		/**
		 * @return sliver
		 */
		public BufferedImage getSliver() {
			return sliver;
		}

		/**
		 * @param sliver 要设置的 sliver
		 */
		public void setSliver(BufferedImage sliver) {
			this.sliver = sliver;
		}

		/**
		 * @return csx
		 */
		public int getCsx() {
			return csx;
		}

		/**
		 * @param csx 要设置的 csx
		 */
		public void setCsx(int csx) {
			this.csx = csx;
		}

		/**
		 * @return csy
		 */
		public int getCsy() {
			return csy;
		}

		/**
		 * @param csy 要设置的 csy
		 */
		public void setCsy(int csy) {
			this.csy = csy;
		}

		/**
		 * @return coins
		 */
		public BufferedImage[] getCoins() {
			return coins;
		}

		/**
		 * @param coins 要设置的 coins
		 */
		public void setCoins(BufferedImage[] coins) {
			this.coins = coins;
		}

		/**
		 * @return coin1
		 */
		public BufferedImage getCoin1() {
			return coin1;
		}

		/**
		 * @param coin1 要设置的 coin1
		 */
		public void setCoin1(BufferedImage coin1) {
			this.coin1 = coin1;
		}

		/**
		 * @return coin2
		 */
		public BufferedImage getCoin2() {
			return coin2;
		}

		/**
		 * @param coin2 要设置的 coin2
		 */
		public void setCoin2(BufferedImage coin2) {
			this.coin2 = coin2;
		}

		/**
		 * @return coin
		 */
		public BufferedImage getCoin() {
			return coin;
		}

		/**
		 * @param coin 要设置的 coin
		 */
		public void setCoin(BufferedImage coin) {
			this.coin = coin;
		}

		/**
		 * @return cx
		 */
		public int getCx() {
			return cx;
		}

		/**
		 * @param cx 要设置的 cx
		 */
		public void setCx(int cx) {
			this.cx = cx;
		}

		/**
		 * @return cy
		 */
		public int getCy() {
			return cy;
		}

		/**
		 * @param cy 要设置的 cy
		 */
		public void setCy(int cy) {
			this.cy = cy;
		}

		/**
		 * @return index
		 */
		public int getIndex() {
			return index;
		}

		/**
		 * @param index 要设置的 index
		 */
		public void setIndex(int index) {
			this.index = index;
		}

		/**
		 * @return speed
		 */
		public int getSpeed() {
			return speed;
		}

		/**
		 * @param speed 要设置的 speed
		 */
		public void setSpeed(int speed) {
			this.speed = speed;
		}

		/**
		 * @return random
		 */
		public Random getRandom() {
			return random;
		}

		/**
		 * @param random 要设置的 random
		 */
		public void setRandom(Random random) {
			this.random = random;
		}
	
		
		
}














