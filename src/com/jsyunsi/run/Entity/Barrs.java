/**
 * 
 */
package com.jsyunsi.run.Entity;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;

import com.jsyunsi.run.JFrame.GameFrame;

/**
 * @ 陈东
 * @ 2018年7月13日
 * @ 下午4:07:53
 * @ 日常搬砖
 */
public class Barrs {
	private int x,y;			//怪物的坐标
	private int a,b;			//悬浮障碍物的坐标
	int dx,dy;					//导弹的坐标
	int sx,sy;					//台阶的坐标
	private BufferedImage[] images;
	private BufferedImage image;
	private BufferedImage bar1,bar2;
	private BufferedImage fly;
	private BufferedImage flys[];
	private BufferedImage f1,f2,f3,f4;
	private BufferedImage guide;
	private BufferedImage guide2;
	private BufferedImage stage;
	private int index;
	private int speed;
	public static final int WIDTH = 120;
	public static final int HEIGHT = 160;
	Random random = new Random();
	
	public Barrs() {
		initBarrs();
		images = new BufferedImage[] {bar1,bar2,f1,f2,f3,f4};
		flys = new BufferedImage[] {f1,f2,f3,f4};
		fly = flys[random.nextInt(4)];
		index = 0;
		speed = 2;
		x = GameFrame.WIDTH + random.nextInt(500);
		y = 430-HEIGHT;
		a = GameFrame.WIDTH + random.nextInt(2000);
		dx = GameFrame.WIDTH + random.nextInt(2222);
		dy = random.nextInt(330-HEIGHT);
		sx = GameFrame.WIDTH + random.nextInt(1666);
		sy = 300;
		
	}
	
	//移动方法
	public void step() {
		image = images[index++/80%2];
		//coin = coins[index++/33%2];
		x -= speed;
		a -= speed; 
		sx -= speed;
		dx -= speed*6;

	}
	
	//绘制
	public void paintBarrs(Graphics g) {
		
		
		g.drawImage(image, x, y+79, WIDTH/2, HEIGHT/2, null);
		g.drawImage(fly, a, b,77,158,null);
		g.drawImage(stage, sx, sy, null);
		g.drawImage(guide, dx, dy,null);

	}
	
	private void initBarrs() {
		try {
			bar1 = ImageIO.read(new File("image/a4.png"));
			bar2 = ImageIO.read(new File("image/a2.png"));
			f1 = ImageIO.read(new File("image/11.png"));
			f2 = ImageIO.read(new File("image/12.png"));
			f3 = ImageIO.read(new File("image/13.png"));
			f4 = ImageIO.read(new File("image/14.png"));
			guide = ImageIO.read(new File("image/daodan.png"));
			//guide2 = ImageIO.read(new File("image/daodanup.png"));
			stage = ImageIO.read(new File("image/hhh.png"));
			
		} catch (IOException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
	}

	/**
	 * @return x
	 */
	public int getX() {
		return x;
	}

	/**
	 * @return guide2
	 */
	public BufferedImage getGuide2() {
		return guide2;
	}

	/**
	 * @param guide2 要设置的 guide2
	 */
	public void setGuide2(BufferedImage guide2) {
		this.guide2 = guide2;
	}

	
	/**
	 * @param x 要设置的 x
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * @return y
	 */
	public int getY() {
		return y;
	}

	/**
	 * @param y 要设置的 y
	 */
	public void setY(int y) {
		this.y = y;
	}

	/**
	 * @return images
	 */
	public BufferedImage[] getImages() {
		return images;
	}

	/**
	 * @param images 要设置的 images
	 */
	public void setImages(BufferedImage[] images) {
		this.images = images;
	}

	/**
	 * @return image
	 */
	public BufferedImage getImage() {
		return image;
	}

	/**
	 * @param image 要设置的 image
	 */
	public void setImage(BufferedImage image) {
		this.image = image;
	}

	/**
	 * @return bar1
	 */
	public BufferedImage getBar1() {
		return bar1;
	}

	/**
	 * @param bar1 要设置的 bar1
	 */
	public void setBar1(BufferedImage bar1) {
		this.bar1 = bar1;
	}

	/**
	 * @return bar2
	 */
	public BufferedImage getBar2() {
		return bar2;
	}

	/**
	 * @param bar2 要设置的 bar2
	 */
	public void setBar2(BufferedImage bar2) {
		this.bar2 = bar2;
	}

	/**
	 * @return index
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * @param index 要设置的 index
	 */
	public void setIndex(int index) {
		this.index = index;
	}

	/**
	 * @return speed
	 */
	public int getSpeed() {
		return speed;
	}

	/**
	 * @param speed 要设置的 speed
	 */
	public void setSpeed(int speed) {
		this.speed = speed;
	}

	/**
	 * @return random
	 */
	public Random getRandom() {
		return random;
	}

	/**
	 * @param random 要设置的 random
	 */
	public void setRandom(Random random) {
		this.random = random;
	}

	/**
	 * @return width
	 */
	public static int getWidth() {
		return WIDTH;
	}

	/**
	 * @return height
	 */
	public static int getHeight() {
		return HEIGHT;
	}

	/**
	 * @return a
	 */
	public int getA() {
		return a;
	}

	/**
	 * @param a 要设置的 a
	 */
	public void setA(int a) {
		this.a = a;
	}

	/**
	 * @return b
	 */
	public int getB() {
		return b;
	}

	/**
	 * @param b 要设置的 b
	 */
	public void setB(int b) {
		this.b = b;
	}

	/**
	 * @return dx
	 */
	public int getDx() {
		return dx;
	}

	/**
	 * @param dx 要设置的 dx
	 */
	public void setDx(int dx) {
		this.dx = dx;
	}

	/**
	 * @return dy
	 */
	public int getDy() {
		return dy;
	}

	/**
	 * @param dy 要设置的 dy
	 */
	public void setDy(int dy) {
		this.dy = dy;
	}

	/**
	 * @return sx
	 */
	public int getSx() {
		return sx;
	}

	/**
	 * @param sx 要设置的 sx
	 */
	public void setSx(int sx) {
		this.sx = sx;
	}

	/**
	 * @return sy
	 */
	public int getSy() {
		return sy;
	}

	/**
	 * @param sy 要设置的 sy
	 */
	public void setSy(int sy) {
		this.sy = sy;
	}

	/**
	 * @return fly
	 */
	public BufferedImage getFly() {
		return fly;
	}

	/**
	 * @param fly 要设置的 fly
	 */
	public void setFly(BufferedImage fly) {
		this.fly = fly;
	}

	/**
	 * @return flys
	 */
	public BufferedImage[] getFlys() {
		return flys;
	}

	/**
	 * @param flys 要设置的 flys
	 */
	public void setFlys(BufferedImage[] flys) {
		this.flys = flys;
	}

	/**
	 * @return f1
	 */
	public BufferedImage getF1() {
		return f1;
	}

	/**
	 * @param f1 要设置的 f1
	 */
	public void setF1(BufferedImage f1) {
		this.f1 = f1;
	}

	/**
	 * @return f2
	 */
	public BufferedImage getF2() {
		return f2;
	}

	/**
	 * @param f2 要设置的 f2
	 */
	public void setF2(BufferedImage f2) {
		this.f2 = f2;
	}

	/**
	 * @return f3
	 */
	public BufferedImage getF3() {
		return f3;
	}

	/**
	 * @param f3 要设置的 f3
	 */
	public void setF3(BufferedImage f3) {
		this.f3 = f3;
	}

	/**
	 * @return f4
	 */
	public BufferedImage getF4() {
		return f4;
	}

	/**
	 * @param f4 要设置的 f4
	 */
	public void setF4(BufferedImage f4) {
		this.f4 = f4;
	}

	/**
	 * @return guide
	 */
	public BufferedImage getGuide() {
		return guide;
	}

	/**
	 * @param guide 要设置的 guide
	 */
	public void setGuide(BufferedImage guide) {
		this.guide = guide;
	}

	/**
	 * @return stage
	 */
	public BufferedImage getStage() {
		return stage;
	}

	/**
	 * @param stage 要设置的 stage
	 */
	public void setStage(BufferedImage stage) {
		this.stage = stage;
	}
	
	
	
}
