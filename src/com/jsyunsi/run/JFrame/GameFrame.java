/**
 * 
 */
package com.jsyunsi.run.JFrame;

import java.applet.Applet;
import java.applet.AudioClip;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

import com.jsyunsi.run.JFrame.GameOver.gameoverJPanel;
import com.jsyunsi.run.JPanel.GamePanel;

/**
 * @ 陈东
 * @ 2018年7月13日
 * @ 上午9:08:45
 * @ 日常搬砖
 */
//主程序界面窗体
public class GameFrame extends JFrame{
	
	public static final int WIDTH = 1000;
	public static final int HEIGHT = 550;
	GamePanel panel = new GamePanel(); 
	
	//加载音乐
		File file;
		URL url;
		URI uri;
		AudioClip  game;
	
	public GameFrame() {
		
		try {
			file = new File("sound/game.wav");
			uri = file.toURI();
			url = uri.toURL();
			game = Applet.newAudioClip(url);
			game.loop();
		} catch (MalformedURLException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		
		//panel = new GamePanel();
		panel.action();
		this.add(panel);
		this.addMouseListener(panel);
		this.addKeyListener(panel);
		this.setSize(WIDTH,HEIGHT);
		this.setUndecorated(true);
		this.setIconImage(new ImageIcon("image/115.png").getImage());
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		while(true) {
			if(panel.isover == true) {
				dispose();
				game.stop();
			}
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				// TODO 自动生成的 catch 块
				e.printStackTrace();
			}
		}
	}
	
	public static void main(String[] args) {
		new GameFrame();
		
	}

}
