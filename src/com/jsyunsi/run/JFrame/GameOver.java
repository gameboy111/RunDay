/**
 * 
 */
package com.jsyunsi.run.JFrame;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.jsyunsi.run.Entity.Person;

/**
 * @ 陈东
 * @ 2018年7月16日
 * @ 上午10:35:50
 * @ 日常搬砖
 */
public class GameOver extends JFrame implements MouseListener{

	Image overPicture;
	JLabel ReturnMenu;
	JLabel ReStart;
	
	String endDistance;
	String endScore;
	
	
	public GameOver(int a,int b) {
		gameoverJPanel g = new gameoverJPanel(a,b);
		
		
		
		//返回主菜单按钮
		ReturnMenu = new JLabel(new ImageIcon("image/hh6.png"));
		ReturnMenu.setBounds(130, 450, 120, 50);
		//设置控件不可用
		ReturnMenu.setEnabled(false);
		ReturnMenu.addMouseListener(this);
		this.add(ReturnMenu);
		
		//再来一局按钮
		ReStart = new JLabel(new ImageIcon("image/hh5.png"));
		ReStart.setBounds(380, 450, 120, 50);
		//设置控件不可用
		ReStart.setEnabled(false);
		ReStart.addMouseListener(this);
		this.add(ReStart);
		
		this.add(g);
		this.setSize(1000,550);
		this.setTitle("gameover");
		this.setUndecorated(true);
		this.setIconImage(new ImageIcon("image/115.png").getImage());
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	public static void main(String[] args) {
		
		new GameOver(1,1);
	}
	
	public class gameoverJPanel extends JPanel {

		public gameoverJPanel(int a,int b) {
			overPicture = new ImageIcon("image/endbg.png").getImage();
			endDistance = " " + (a);
			endScore = " " + (b);
		}

		public void paint(Graphics g) {
			super.paint(g);
			g.drawImage(overPicture, 0, 0,1000,550, null);

			Font f2 = new Font("宋体",Font.BOLD,30);
			g.setFont(f2);
			g.setColor(Color.white);
			g.drawString(endDistance,710,383);
			
			Font f = new Font("宋体",Font.BOLD,30);
			g.setFont(f);
			g.setColor(Color.white);
			g.drawString(endScore,710,435);
		}
		
		
	}

	public void mouseClicked(MouseEvent e) {
		if(e.getSource().equals(ReturnMenu)){
			dispose();
			new MenuFrame();
			
		}
		if(e.getSource().equals(ReStart)){
			
			//new WindowFrame().Start();
			new GameFrame();
			dispose();
		}
	}

	public void mouseEntered(MouseEvent e) {
		if(e.getSource().equals(ReturnMenu)){
			ReturnMenu.setEnabled(true);
		}
		if(e.getSource().equals(ReStart)){
			ReStart.setEnabled(true);
		}
	}

	public void mouseExited(MouseEvent e) {
		if(e.getSource().equals(ReturnMenu)){
			ReturnMenu.setEnabled(false);
		}
		if(e.getSource().equals(ReStart)){
			ReStart.setEnabled(false);
		}
	}

	public void mousePressed(MouseEvent arg0) {
		// TODO 自动生成的方法存根
		
	}

	public void mouseReleased(MouseEvent arg0) {
		// TODO 自动生成的方法存根
		
	}
	
}









