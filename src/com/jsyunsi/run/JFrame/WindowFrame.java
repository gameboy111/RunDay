/**
 * 
 */
package com.jsyunsi.run.JFrame;

import java.awt.BorderLayout;
import java.awt.Color;
import java.io.IOException;
import java.nio.CharBuffer;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JProgressBar;

/**
 * @ 陈东
 * @ 2018年7月11日
 * @ 下午4:22:28
 * @ 日常搬砖
 */
public class WindowFrame extends JFrame implements Runnable{
	//背景图片
	JLabel backImage;
	//进度条
	JProgressBar jdt;		//进度条
	
	public WindowFrame() {
		backImage = new JLabel(new ImageIcon("image/hbg.jpg"));
		jdt = new JProgressBar();
		//jdt.setBackground(Color.orange);			//设置进度条颜色
		jdt.setStringPainted(true); 				//设置进度条内容为字符串
		this.add(backImage,BorderLayout.NORTH);
		this.add(jdt,BorderLayout.SOUTH);
		
		//窗体属性设置
		this.setSize(756,444);
		this.setLocationRelativeTo(null);
		this.setIconImage(new ImageIcon("image/115.png").getImage());
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setUndecorated(true);
		this.setVisible(true);
		
	}
	
	public static void main(String[] args) {
		// TODO 自动生成的方法存根
		new WindowFrame().Start();

	}

	public void Start() {
		WindowFrame window = new WindowFrame();
		Thread t = new Thread(window);
		t.start();
		dispose();
	}
	
	public void run() {
		int [] progress = {1,5,10,20,35,45,55,70,80,100};	//设置进度条内容
		for(int i = 0;i < progress.length;i++) {
			
			
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO 自动生成的 catch 块
				e.printStackTrace();
			}
			jdt.setValue(progress[i]);
		}
		dispose();
		new GameFrame();
		
	}

}
